//
// Created by kir on 08.03.18.
//

#ifndef FGET_HTTP_PARSER_H
#define FGET_HTTP_PARSER_H

#include <stdio.h>
#include "url_info.h"

typedef int (*body_chunk_process_cb_f)(void *ctx, const char *data, int data_size);
typedef enum { STATE_REQ, STATE_HDRS, STATE_BODY, STATE_DONE } http_parser_state_t;
typedef enum { TRANSFER_DEFAULT } http_transfer_encoding_t;

#define HTTP_REASON_MAX 512U


struct http_parser_t {
    FILE *stream;
    http_transfer_encoding_t transfer_encoding;
    http_parser_state_t parser_state;
    int status_code;
    char reason[HTTP_REASON_MAX];
    char version[4];
    body_chunk_process_cb_f body_chunk_process_cb;
    void *cb_ctx;
};

struct http_parser_t *http_parser_construct(struct url_info_t *url_info,
                                            body_chunk_process_cb_f body_chunk_process_cb, void *cb_ctx);
void http_parser_destruct(struct http_parser_t *http_parser);
int http_parser_get(struct http_parser_t *http_parser, struct url_info_t *url_info);

#endif //FGET_HTTP_PARSER_H
