## fget - file HTTP downloader ##
```
Usage: ./fget [ARGUMENTS] http://host[:port][/path]
Available arguments:
   -d,--log_level NUMBER      log level (optional, default 0)

   -o,--output                output file name (optional)
```
