//
// Created by kir on 08.03.18.
//

#include "log.h"
#include <stdarg.h>

long log_level = 0;

void fget_log(FILE *dev, const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vfprintf(dev, fmt, args);
    va_end(args);
    fprintf(dev, "\n");
}