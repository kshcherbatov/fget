#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include "log.h"
#include "url_info.h"
#include "http_parser.h"

static const char *output_filename = NULL;

static void usage(const char *prg_name, const char *err_msg)
{
    if (err_msg)
        printf("%s\n", err_msg);

    printf("Usage: %s [ARGUMENTS] http://host[:port][/path]\n"
           "Available arguments:\n"
           "   -d,--log_level NUMBER      log level (optional, default 0)\n\n"
           "   -o,--output                output file name (optional)\n\n",
           prg_name);
}

static int parse_number(const char *str, long *res)
{
    char *endptr = 0;
    errno = 0;
    *res = strtol(str, &endptr, 10);
    return (endptr == str || *endptr != '\0'
            || errno == ERANGE);
}

static int parse_options(int argc, char **argv)
{
    const char *prg_name = argv[0];

    const struct option long_options[] = {
            {"log_level", required_argument, 0,  'd' },
            {"output",    required_argument, 0,  'o' },
            {NULL,        0,                 0,   0  }
    };
    const char *short_options = "d:o:";

    int ch = 0;
    while ((ch = getopt_long(argc, argv, short_options, long_options, NULL)) != -1) {
        switch (ch) {
            case 'd':
                if (parse_number(optarg, &log_level)) {
                    printf("Invalid log_level argument '%s'!\n", optarg);
                    usage(prg_name, 0);
                    return -1;
                }
                break;
            case 'o':
                output_filename = optarg;
                break;
            default:
                usage(prg_name, "Unknown argument");
                return -1;
        }
    }
    return 0;
}

static int fget_print_screen_cb(void *ctx, const char *data, int data_size)
{
    FILE *fd = (FILE *)ctx;
    size_t done = fwrite(data, 1, (size_t )data_size, fd);
    return  done != data_size;
}

static FILE *get_output_file(struct url_info_t *url_info)
{
    FILE *output = fopen(output_filename ? output_filename : url_info->filename, "w+");
    if (output == NULL) {
        fget_log_error("Can't open stream for output file: %s", strerror(errno));
        return NULL;
    }
    return output;
}

static void make_get_request(struct url_info_t *url_info)
{
    FILE *output = get_output_file(url_info);
    struct http_parser_t *http_parser;
    if (!output ||
            !(http_parser = http_parser_construct(url_info, fget_print_screen_cb, output)) ||
            http_parser_get(http_parser, url_info)) {
        printf("Task failed..\n");
    } else {
        printf("Task done!..\n");
    }
    if (output)
        fclose(output);
    http_parser_destruct(http_parser);
}

int main(int argc, char *argv[]) {
    if (parse_options(argc, argv))
        return 1;
    
    if (optind + 1 > argc) {
        usage(argv[0], "Too few input parameters!");
        return 1;
    }
    struct url_info_t url_info;
    if (url_info_by_url(argv[optind], &url_info)) {
        usage(argv[0], "Invalid URL specified!");
        return 1;
    }

    make_get_request(&url_info);

    return 0;
}