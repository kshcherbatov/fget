//
// Created by kir on 08.03.18.
//


#include "log.h"
#include "sock.h"

#include <assert.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>


static int fget_sock_by_addr(const char *sin_addr, int sin_addr_len,  in_port_t port_norder)
{
    assert(sin_addr_len == 4); /* IPv4 address */

    struct sockaddr_in sock_addr = {.sin_port = port_norder, .sin_family = AF_INET};
    strncpy((char *)&sock_addr.sin_addr, sin_addr, (size_t)sin_addr_len);

    int fd = -1;
    if ((fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        fget_log_error("socket(PF_INET, SOCK_STREAM, IPPROTO_TCP): %s", strerror(errno));
        return -1;
    }

    if(connect(fd, (struct sockaddr *)&sock_addr, sizeof(struct sockaddr_in)) == -1) {
        if (errno == ECONNREFUSED || errno == ENETUNREACH || errno == ETIMEDOUT) {
            fget_log_info("connect(): %s", strerror(errno));
        } else {
            fget_log_error("connect(): %s", strerror(errno));
        }
        goto __clean_and_error;
    }

    return fd;

    __clean_and_error:
    close(fd);
    return -1;
}

int fget_sock_open(struct url_info_t *url_info)
{
    struct hostent *hp = NULL;
    if ((hp = gethostbyname(url_info->hostname)) == NULL) {
        fget_log_error("Can't resolve hp '%s': %s", url_info->hostname, hstrerror(h_errno));
        return -1;
    }
    if (hp->h_addrtype != AF_INET) {
        // FIXME: Make IPv6 support =)
        fget_log_error("IPv6 is not implemented yet!");
        return -1;
    }
    in_port_t port_norder = htons(url_info->port);
    int idx = 0, fd = -1;
    while (hp->h_addr_list[idx] &&
           ((fd = fget_sock_by_addr(hp->h_addr_list[idx], hp->h_length, port_norder)) < 0)) {
        fget_log_info("Can't establish connection to '%s:%d'",
                      inet_ntoa(*(struct in_addr *)hp->h_addr_list[idx]), url_info->port);
        idx++;
    }

    return fd;
}

int fget_sock_close(int fd)
{
    int rc = 0;
    if (shutdown(fd, SHUT_RDWR) < 0) {
        fget_log_error("shutdown(): %s", strerror(errno));
        rc |= -1;
    }
    if (close(fd) < 0) {
        fget_log_error("close(): %s", strerror(errno));
        rc |= -1;
    }
    return rc;
}