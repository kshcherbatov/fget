CC=gcc
CFLAGS=-I.
DEPS = url_info.h log.h http_parser.h sock.h
OBJ = main.o  log.o http_parser.o url_info.o sock.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

fget: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o fget
