//
// Created by kir on 08.03.18.
//

#include <memory.h>
#include <stdlib.h>
#include <errno.h>
#include "url_info.h"
#include "log.h"

int url_info_by_url(const char *original_url, struct url_info_t *url_info)
{
    int rc = 0;
    char *url_begin = NULL, *url = NULL;
    url = url_begin = strdup(original_url);

    /* clean '/' at the end if present */
    url = strrchr(url, '/');
    if (url && *(url+1) == '\0')
        *url = '\0';
    url = url_begin;

    char *data = NULL;
    char *temp = url;
    while ((temp = strstr(temp+1, "://")) != NULL)
        data = temp;
    if (data && strncmp(url, "http://", data - url)) {
        fget_log_error("Invalid shema: '%.*s'", data-url, url);
        rc = -1;
        goto __clean;
    }
    url_info->port = (uint16_t)80;
    if (data)
        url = data + strlen("://");

    const char *column = strstr(url, ":");
    const char *slash = strstr(url, "/");
    if (column > slash || slash == url) {
        fget_log_error("Invalid address hostname");
        rc = -1;
        goto __clean;
    }
    if (column) {
        char *end;
        long int res = strtol(column + 1, &end, 10);
        if (errno == ERANGE || errno == EINVAL || res <= 0 || res >= 1024) {
            fget_log_error("Invalid port specified");
            rc = -1;
            goto __clean;
        }
        url_info->port = (uint16_t)res;
        snprintf(url_info->path, MAX_PATH_LEN, "%s", end);
        snprintf(url_info->hostname,  column-url+1, "%s", url);
    } else if (slash) {
        snprintf(url_info->hostname,  slash-url+1, "%s", url);
        snprintf(url_info->path,  MAX_PATH_LEN, "%s", slash);
    } else {
        snprintf(url_info->hostname,  MAX_HOSTNAME_LEN, "%s", url);
    }

    data = strrchr(url, '/');
    if (data)
        url = data+1;
    snprintf(url_info->filename,  MAX_FILENAME_LEN, "%s", url);

__clean:
    free(url_begin);
    return rc;
}