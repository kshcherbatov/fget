//
// Created by kir on 08.03.18.
//

#ifndef FGET_SOCK_H
#define FGET_SOCK_H

#include "url_info.h"

int fget_sock_open(struct url_info_t *url_info);
int fget_sock_close(int fd);

#endif //FGET_SOCK_H
