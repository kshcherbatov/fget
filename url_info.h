//
// Created by kir on 08.03.18.
//

#ifndef FGET_URL_INFO_H
#define FGET_URL_INFO_H

#include <netinet/in.h>

#define MAX_HOSTNAME_LEN 256
#define MAX_FILENAME_LEN 256
#define MAX_PATH_LEN 4096

struct url_info_t {
    char hostname[MAX_HOSTNAME_LEN];
    in_port_t port;
    char path[MAX_PATH_LEN];
    char filename[MAX_FILENAME_LEN];
};

int url_info_by_url(const char *url, struct url_info_t *url_info);

#endif //FGET_URL_INFO_H
