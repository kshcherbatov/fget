//
// Created by kir on 08.03.18.
//

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <assert.h>
#include <memory.h>
#include <errno.h>
#include "http_parser.h"
#include "sock.h"
#include "log.h"

#define URL_LEN_MAX 2083U
#define HTTP_STATUS_OK 200

struct http_parser_t *http_parser_construct(struct url_info_t *url_info,
                                            body_chunk_process_cb_f body_chunk_process_cb,
                                            void *cb_ctx)
{
    int sock_fd = 0;
    FILE *stream;
    if (((sock_fd = fget_sock_open(url_info)) < 0) ||
            ((stream = fdopen(sock_fd, "w+")) == NULL)) {
        fget_log_error("Can't construct http_parser. Failed establish server connection");
        if (sock_fd > 0)
            fget_sock_close(sock_fd);
        return NULL;
    }
    struct http_parser_t *http_parser = calloc(1, sizeof(struct http_parser_t));
    if (!http_parser) {
        fget_log_error("malloc(): Can't allocate memory");
        return NULL;
    }
    http_parser->body_chunk_process_cb = body_chunk_process_cb;
    http_parser->cb_ctx = cb_ctx;
    http_parser->parser_state = STATE_REQ;
    http_parser->stream = stream;
    http_parser->transfer_encoding = TRANSFER_DEFAULT;
    return http_parser;
}

void http_parser_destruct(struct http_parser_t *http_parser)
{
    if (!http_parser)
        return;
    if (fclose(http_parser->stream)) {
        fget_log_error("fclose(): %s", strerror(errno));
    }
    free(http_parser);
}

static int http_parser_send(struct http_parser_t *http_parser, struct url_info_t *url_info)
{
    void *prev = signal(SIGPIPE, SIG_IGN);
    ssize_t rc = 0;
    int ret = 0;
    static const char get_request_fmt[] =
            "GET http://%s:%d%s HTTP/1.1\r\n"
            "Host: %s\r\n"
            "Connection: close\r\n"
            "\r\n";
    static const size_t get_request_str_len_max = sizeof(get_request_fmt) + URL_LEN_MAX;
    char request_str[get_request_str_len_max];
    assert(strlen(url_info->hostname) + strlen(url_info->path) <= URL_LEN_MAX); // NOTE: check this on arguments scan
    int get_request_str_len = snprintf(request_str, get_request_str_len_max,
                                       get_request_fmt,
                                       url_info->hostname, url_info->port, url_info->path,
                                       url_info->hostname);
    assert(get_request_str_len > 0 && get_request_str_len <= get_request_str_len_max);
    fget_log_debug("Make HTTP Request:\n%s", request_str);

    if ((rc = fprintf(http_parser->stream, request_str, (size_t)get_request_str_len)) != get_request_str_len) {
        fget_log_error("fprintf(): rc=%lld, %s", rc, strerror(errno));
        ret = -1;
        goto __clean;
    }
    http_parser->parser_state = STATE_HDRS;

__clean:
    signal(SIGPIPE, prev);
    return ret;
}

static int http_parser_recieve_meta(struct http_parser_t *http_parser, struct url_info_t *url_info)
{
    assert(http_parser->parser_state == STATE_HDRS);

    void *prev = signal(SIGPIPE, SIG_IGN);
    ssize_t rc = 0;
    int ret = 0;
    if ((rc = fprintf(http_parser->stream, "\r\n") != 2) ||
            fflush(http_parser->stream) == EOF) {
        fget_log_error("fprintf(): rc=%lld, %s", rc, strerror(errno));
        signal(SIGPIPE, prev);
        return -1;
    }
    rewind(http_parser->stream);
    signal(SIGPIPE, prev);
    if (fscanf(http_parser->stream, "HTTP/%3s %d %511s\r\n",
               http_parser->version, &http_parser->status_code, http_parser->reason) != 3 ||
            http_parser->status_code != HTTP_STATUS_OK) {
        fget_log_error("Bad HTTP Response...");
        ret = -1;
        goto __clean;
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    while ((read = getline(&line, &len, http_parser->stream)) != -1 &&
            strcmp(line, "\r\n")) {
        if (!strncasecmp(line, "Transfer-Encoding:", strlen("Transfer-Encoding:"))) {
            fget_log_error("Transfer-Encoding headers is not supported yet.");
            return -1;
        }
        fget_log_debug("Header: %s", line);
    };

    if (read == -1)
        ret = -1;

    http_parser->parser_state = STATE_BODY;

__clean:
    return ret;
}

static int http_parser_recieve_data(struct http_parser_t *http_parser, struct url_info_t *url_info)
{
    assert(http_parser->parser_state == STATE_BODY);
    int rc = 0;
    char chunk[1024];
    ssize_t read = 0;
    while ((read = fread(chunk, 1, 1024, http_parser->stream)) > 0 &&
            !(rc = http_parser->body_chunk_process_cb(http_parser->cb_ctx, chunk, (int)read))) {}
    http_parser->parser_state = STATE_DONE;
    return rc;
}

int http_parser_get(struct http_parser_t *http_parser, struct url_info_t *url_info)
{
    if (http_parser_send(http_parser, url_info))
        return -1;
    if (http_parser_recieve_meta(http_parser, url_info))
        return -1;
    if (http_parser_recieve_data(http_parser, url_info)) {
        fget_log_info("Can't recieve and process whole data");
        return -1;
    }
    return 0;
}