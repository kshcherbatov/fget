//
// Created by kir on 08.03.18.
//

#ifndef FGET_LOG_H
#define FGET_LOG_H

#include <stdio.h>

extern long log_level;

enum {
    LOG_LVL_DEBUG = 2,
    LOG_LVL_INFO = 1,
    LOG_LVL_ERROR = 0
};

void fget_log(FILE *dev, const char *fmt, ...);

#define fget_log_error(...) \
    do { if (log_level >= LOG_LVL_ERROR) fget_log(stdout, __VA_ARGS__); } while (0);

#define fget_log_debug(...) \
    do { if (log_level >= LOG_LVL_DEBUG) fget_log(stdout, __VA_ARGS__); } while (0);

#define fget_log_info(...) \
    do { if (log_level >= LOG_LVL_INFO) fget_log(stdout, __VA_ARGS__); } while (0);

#endif //FGET_LOG_H
